#include <iostream>
#include <string.h>
#include <stack>
#include <fstream>
#include <string>
#include <map>
#include <list>
#include <strstream>
#include <cmath>
#include "operations.h"
#include "creators.h"
#include <gtest/gtest.h>
using namespace std;

class CALCULATOR
{
private:
    map <string,factory*> sfact;
    streambuf* cinbuf;
    ifstream file;
public:
    CALCULATOR(int argc, char**argv)
    {
        sfact["PUSH"] = new factory_push;
        sfact["POP"] = new factory_pop;
        sfact["PRINT"] = new factory_print;
        sfact["DEFINE"] = new factory_define;
        sfact["+"] = new factory_plus;
        sfact["-"] = new factory_minus;
        sfact["/"] = new factory_div;
        sfact["*"] = new factory_mul;
        sfact["SQRT"] = new factory_sqrt;
        sfact["#"] = new factory_comment;

        cinbuf = cin.rdbuf(); //сохраняем старый буфер
        if(argc == 2)
        {
            file.open(argv[1]);
            if (file.is_open())
            {
                cin.rdbuf(file.rdbuf()); // перенапраляем в файл
            }
        }
    }
    int pars(list<string>* ARGS, stack<double>* NUM_STACK, map<string,double>* DEFINE_MAP)
    {
        string commands;
        string word;
        getline(cin, commands);
        strstream ccmd;
        ccmd << commands;
        while(ccmd.good() && ccmd >> word)
        {/*if(DEFINE_MAP->count(word)>0)
                    {
                        word = to_string((*DEFINE_MAP)[word]);
                    }*/
            ARGS->push_back(word);
        }
        return 0;
    }
    void counting() {
        stack<double> NUM_STACK;
        list<string> ARGS;
        map<string, double> DEFINE_MAP;
        while (!cin.eof()) {
            ARGS.clear();
            pars(&ARGS, &NUM_STACK, &DEFINE_MAP);
            /*try{op->vipolnit(ARGS,NUM_STACK,DEFINE_MAP);}
            catch(invalid_argument&)
                {
                cout << "OSHIBKA";
                }
            }*/
            try {
                if (sfact.count(*(ARGS.begin())) <= 0)
                {
                    throw (invalid_argument("unknown operation"));
                }
                Calc_op *op = sfact[*(ARGS.begin())]->make_op();
                ARGS.pop_front();
                op->vipolnit(&ARGS, &NUM_STACK, &DEFINE_MAP);

            }
            catch(invalid_argument& err)
            {
                cout << err.what();
            }
        }
    }
    ~CALCULATOR()
    {
       cin.rdbuf(cinbuf);
       file.close();
    }

};

int main(int argc, char**argv) {
    CALCULATOR CALC1(argc, argv);
    CALC1.counting();
    return RUN_ALL_TESTS();
}

TEST(method,PARS)
{
    strstream file;
    file << "DEFINE answer 42\nPRINT";
    cin.rdbuf(file.rdbuf());
    list<string> ARGS;
    stack<double> NUM_STACK;
    map<string,double> DEFINE_MAP;
    int MYargc=1;
    char** MYargv=NULL;
    CALCULATOR CALC1(MYargc, MYargv);
    CALC1.pars(&ARGS,&NUM_STACK,&DEFINE_MAP);
    EXPECT_EQ(ARGS.front(),"DEFINE");
    ARGS.pop_front();
    EXPECT_EQ(ARGS.front(),"answer");
    ARGS.pop_front();
    EXPECT_EQ(ARGS.front(),"42");
    ARGS.pop_front();
}
TEST(operations, DEFINE)
{
    list<string> ARGS;
    stack<double> NUM_STACK;
    map<string,double> DEFINE_MAP;
    int MYargc=1;
    char** MYargv=NULL;
    ARGS.push_back("answer");
    ARGS.push_back("42");
    OP_DEFINE DEF;
    DEF.vipolnit(&ARGS,&NUM_STACK,&DEFINE_MAP);
    EXPECT_EQ(DEFINE_MAP["answer"],42);
}

TEST(operations, PUSH)
{
    list<string> ARGS;
    stack<double> NUM_STACK;
    map<string,double> DEFINE_MAP;
    int MYargc=1;
    char** MYargv=NULL;
    ARGS.push_back("42");
    OP_PUSH PUSH;
    PUSH.vipolnit(&ARGS,&NUM_STACK,&DEFINE_MAP);
    EXPECT_EQ(NUM_STACK.top(),42);
}

TEST(operations, POP)
{
    list<string> ARGS;
    stack<double> NUM_STACK;
    map<string,double> DEFINE_MAP;
    int MYargc=1;
    char** MYargv=NULL;
    NUM_STACK.push(42);
    NUM_STACK.push(45);
    OP_POP POP;
    POP.vipolnit(&ARGS,&NUM_STACK,&DEFINE_MAP);
    EXPECT_EQ(NUM_STACK.top(),42);
}

TEST(operations, PLUS)
{
    list<string> ARGS;
    stack<double> NUM_STACK;
    map<string,double> DEFINE_MAP;
    int MYargc=1;
    char** MYargv=NULL;
    NUM_STACK.push(45);
    NUM_STACK.push(42);
    OP_PLUS PLUS ;
    PLUS.vipolnit(&ARGS,&NUM_STACK,&DEFINE_MAP);
    EXPECT_EQ(NUM_STACK.top(),87);
}

TEST(operations, MINUS)
{
    list<string> ARGS;
    stack<double> NUM_STACK;
    map<string,double> DEFINE_MAP;
    int MYargc=1;
    char** MYargv=NULL;
    NUM_STACK.push(42);
    NUM_STACK.push(45);
    OP_MINUS MINUS ;
    MINUS.vipolnit(&ARGS,&NUM_STACK,&DEFINE_MAP);
    EXPECT_EQ(NUM_STACK.top(),3);
}

TEST(operations, DIV)
{
    list<string> ARGS;
    stack<double> NUM_STACK;
    map<string,double> DEFINE_MAP;
    int MYargc=1;
    char** MYargv=NULL;
    NUM_STACK.push(2);
    NUM_STACK.push(10);
    OP_DIV DIV ;
    DIV.vipolnit(&ARGS,&NUM_STACK,&DEFINE_MAP);
    EXPECT_EQ(NUM_STACK.top(),5);
}

TEST(operations, MUL)
{
    list<string> ARGS;
    stack<double> NUM_STACK;
    map<string,double> DEFINE_MAP;
    int MYargc=1;
    char** MYargv=NULL;
    NUM_STACK.push(2);
    NUM_STACK.push(10);
    OP_MUL MUL ;
    MUL.vipolnit(&ARGS,&NUM_STACK,&DEFINE_MAP);
    EXPECT_EQ(NUM_STACK.top(), 20);
}

TEST(operations, SQRT)
{
    list<string> ARGS;
    stack<double> NUM_STACK;
    map<string,double> DEFINE_MAP;
    int MYargc=1;
    char** MYargv=NULL;
    NUM_STACK.push(100);
    OP_SQRT SQRT ;
    SQRT.vipolnit(&ARGS,&NUM_STACK,&DEFINE_MAP);
    EXPECT_EQ(NUM_STACK.top(), 10);
}

TEST(operations,PRINT)
{
    strstream Tin;
    strstream Tout;
    streambuf *coutbuf = cout.rdbuf(); //save old buf
    cout.rdbuf(Tout.rdbuf()); //redirect std::cout to out.txt
    streambuf *cinbuf = cin.rdbuf(); //save old buf
    cin.rdbuf(Tin.rdbuf());
    list<string>* ARGS;
    stack<double>* NUM_STACK;
    map<string,double>* DEFINE_MAP;
    OP_PRINT PRINT;
    NUM_STACK->push(42);
    PRINT.vipolnit(ARGS,NUM_STACK,DEFINE_MAP);
    int result;
    Tout >> result;
    EXPECT_EQ(result,42);
    cin.rdbuf(cinbuf);   //reset to standard input again
    cout.rdbuf(coutbuf); //reset to standard output again
}