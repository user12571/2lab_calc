//
// Created by COMPUTER on 26.12.2018.
//


#include <iostream>
#include <string.h>
#include <stack>
#include <fstream>
#include <string>
#include <map>
#include <list>
#include <strstream>
#include <cmath>
#include "operations.h"
using namespace std;

void OP_POP::vipolnit(list<string>* ARGS, stack<double>* NUM_STACK, map<string,double>* DEFINE_MAP)
{
    if(NUM_STACK->empty())
    {
        throw(invalid_argument("stack is empty"));
    }
    NUM_STACK->pop();
    cout <<"Command: pop\n";
}
OP_POP::~OP_POP()
{
}

void OP_PUSH::vipolnit(list<string>* ARGS, stack<double>* NUM_STACK, map<string,double>* DEFINE_MAP)
{
    if((ARGS->size() != 1))
    {
        throw(invalid_argument("unnecessary arguments"));
    }
    double NUM;
    if (DEFINE_MAP->count(*(ARGS->begin())) > 0)
    {
        *(ARGS->begin()) = to_string((*DEFINE_MAP)[*(ARGS->begin())]);
    }
    try{ NUM = stod(*(ARGS->begin())); }
    catch(invalid_argument&)
    {
        //ERROR
        cout << "invalid argument";
        return;
    }
    NUM_STACK->push(NUM);
    cout<<"Command: push "<< NUM << '\n';
}


void OP_PRINT::vipolnit(list<string>* ARGS, stack<double>* NUM_STACK, map<string,double>* DEFINE_MAP)
{
    cout <<"Command: print VALUE \n";
    if(!(ARGS->empty()))
    {
        throw(invalid_argument("unnecessary arguments"));
    }
    if(NUM_STACK->empty())
    {
        throw(invalid_argument("stack is empty"));
    }
    cout << NUM_STACK->top() << '\n';
}

void OP_PLUS::vipolnit(list<string>* ARGS, stack<double>* NUM_STACK, map<string,double>* DEFINE_MAP)
{
    double NUM1;
    double NUM2;
    if(NUM_STACK->size() < 2)
    {
        throw(invalid_argument("not enough arguments in the stack"));
    }
    if(ARGS->size() != 0)
    {
        throw(invalid_argument("unnecessary arguments"));
    }
    NUM1 = NUM_STACK->top();
    NUM_STACK->pop();
    NUM2 = NUM_STACK->top();
    NUM_STACK->pop();
    cout <<"Command: "<< NUM1 << " + " << NUM2 << " = " << NUM1+NUM2 << "\n";
    NUM_STACK->push(NUM1+NUM2);
}


void OP_MINUS::vipolnit(list<string>* ARGS, stack<double>* NUM_STACK, map<string,double>* DEFINE_MAP)
{
    double NUM1;
    double NUM2;
    if(NUM_STACK->size() < 2)
    {
        throw(invalid_argument("not enough arguments in the stack"));
    }
    if(ARGS->size() != 0)
    {
        throw(invalid_argument("unnecessary arguments"));
    }
    NUM1 = NUM_STACK->top();
    NUM_STACK->pop();
    NUM2 = NUM_STACK->top();
    NUM_STACK->pop();
    cout <<"Command: "<< NUM1 << " - " << NUM2 << " = " << NUM1-NUM2 << "\n";
    NUM_STACK->push(NUM1-NUM2);
}

void OP_SQRT::vipolnit(list<string>* ARGS, stack<double>* NUM_STACK, map<string,double>* DEFINE_MAP)
{
    double NUM1;
    if(NUM_STACK->size() < 1)
    {
        throw(invalid_argument("not enough arguments in the stack"));
    }
    if(ARGS->size() != 0)
    {
        throw(invalid_argument("unnecessary arguments"));
    }
    if(NUM_STACK->top() < 0)
    {
        throw(invalid_argument("operation is impossible"));
    }
    NUM1 = NUM_STACK->top();
    NUM_STACK->pop();
    cout <<"Command: "<< "SQRT( " << NUM1 << " ) = " << sqrt(NUM1) <<'\n';
    NUM1 = sqrt(NUM1);
    if(NUM1 == NAN)
    {
        cout << "invalid argument";
        throw(invalid_argument("operation is impossible"));
    }
    NUM_STACK->push(NUM1);
}


void OP_MUL::vipolnit(list<string>* ARGS, stack<double>* NUM_STACK, map<string,double>* DEFINE_MAP)
{
    double NUM1;
    double NUM2;
    if(NUM_STACK->size() < 2)
    {
        throw(invalid_argument("not enough arguments in the stack"));
    }
    if(ARGS->size() != 0)
    {
        throw(invalid_argument("unnecessary arguments"));
    }
    NUM1 = NUM_STACK->top();
    NUM_STACK->pop();
    NUM2 = NUM_STACK->top();
    NUM_STACK->pop();
    cout <<"Command: "<< NUM1 << " * " << NUM2 << " = " << NUM1*NUM2 << "\n";
    NUM_STACK->push(NUM1*NUM2);
}


void OP_DIV::vipolnit(list<string>* ARGS, stack<double>* NUM_STACK, map<string,double>* DEFINE_MAP)
{
    double NUM1;
    double NUM2;
    if(NUM_STACK->size() < 2)
    {
        throw(invalid_argument("not enough arguments in the stack"));
    }
    if(ARGS->size() != 0)
    {
        throw(invalid_argument("unnecessary arguments"));
    }
    NUM1 = NUM_STACK->top();
    NUM_STACK->pop();
    if(NUM_STACK->top() == 0.0 /*|| ARGS->begin()->front() == NaN || ARGS->begin()->front() == INFINITY*/)
    {
        NUM_STACK->push(NUM1);
        throw(invalid_argument("operation is impossible"));
    }
    NUM2 = NUM_STACK->top();
    NUM_STACK->pop();
    cout <<"Command: "<< NUM1 << " / " << NUM2 << " = " << NUM1/NUM2 << "\n";
    NUM_STACK->push(NUM1/NUM2);
}


void OP_DEFINE::vipolnit(list<string>* ARGS, stack<double>* NUM_STACK, map<string,double>* DEFINE_MAP)
{
    double NUM;
    string DEF_STR;
    if(ARGS->size() < 2)
    {
        throw(invalid_argument("not enough arguments"));
    }
    DEF_STR=*(ARGS->begin());
    ARGS->pop_front();
    //try{ NUM = stod(*(ARGS->begin())); /*throw(not_string);*/}
    //catch(invalid_argument&)
    //{
    //    DEF_STR=*(ARGS->begin());
    //}
    //иначе ERROR , cout << "invalid argument 1" return
    //ARGS->pop_front();
    try{ NUM = stod(*(ARGS->begin())); }
    catch(invalid_argument&)
    {
        //ERROR
        cout << "invalid argument 2";
        return;
    }
    ARGS->pop_front();
    (*DEFINE_MAP)[DEF_STR]=NUM;
    cout <<"Command: def " << DEF_STR <<" "<< (*DEFINE_MAP)[DEF_STR] << '\n';
}