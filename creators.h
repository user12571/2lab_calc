//
// Created by COMPUTER on 26.12.2018.
//

#ifndef LAB2_CALC_CREATORS_H
#define LAB2_CALC_CREATORS_H

class factory
{
public:
    virtual  Calc_op* make_op() = 0;
    virtual ~factory() = default;
};

class factory_pop: public factory{
public:
    Calc_op* make_op()override
    {
        return new OP_POP();
    }
};

class factory_div: public factory{
public:
    Calc_op* make_op()override
    {
        return new OP_DIV();
    }
};

class factory_mul: public factory{
public:
    Calc_op* make_op()override
    {
        return new OP_MUL();
    }
};

class factory_minus: public factory{
public:
    Calc_op* make_op()override
    {
        return new OP_MINUS();
    }
};

class factory_sqrt: public factory{
public:
    Calc_op* make_op()override
    {
        return new OP_SQRT();
    }
};

class factory_push: public factory{
public:
    Calc_op* make_op()override
    {
        return new OP_PUSH();
    }
};

class factory_define: public factory{
public:
    Calc_op* make_op()override
    {
        return new OP_DEFINE();
    }
};

class factory_print: public factory{
public:
    Calc_op* make_op()override
    {
        return new OP_PRINT();
    }
};

class factory_plus: public factory{
public:
    Calc_op* make_op()override
    {
        OP_POP t;
        return new OP_PLUS();
    }
};
class factory_comment: public factory{
public:
    Calc_op* make_op()override
    {
        return new OP_COMMENT();
    }
};

#endif //LAB2_CALC_CREATORS_H
