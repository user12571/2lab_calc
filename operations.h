//
// Created by COMPUTER on 26.12.2018.
//

#ifndef LAB2_CALC_OPERATIONS_H
#define LAB2_CALC_OPERATIONS_H

#include <iostream>
#include <string.h>
#include <stack>
#include <fstream>
#include <string>
#include <map>
#include <list>
#include <strstream>
#include <cmath>

using namespace std;
class Calc_op
{
public:
    virtual void vipolnit(list<string>* ARGS, stack<double>* NUM_STACK, map<string,double>* DEFINE_MAP)=0;
    virtual ~Calc_op() = default;
};

class OP_POP: public   Calc_op
{
public:
    void vipolnit(list<string>* ARGS, stack<double>* NUM_STACK, map<string,double>* DEFINE_MAP);
    ~OP_POP();
};


class OP_PUSH: public  Calc_op
{
public:
    void vipolnit(list<string>* ARGS, stack<double>* NUM_STACK, map<string,double>* DEFINE_MAP);
};

class OP_PRINT: public  Calc_op
{
public:
    void vipolnit(list<string>* ARGS, stack<double>* NUM_STACK, map<string,double>* DEFINE_MAP);
};

class OP_PLUS: public  Calc_op
{
public:
    void vipolnit(list<string>* ARGS, stack<double>* NUM_STACK, map<string,double>* DEFINE_MAP);
};

class OP_COMMENT: public  Calc_op
{
public:
    void vipolnit(list<string>* ARGS, stack<double>* NUM_STACK, map<string,double>* DEFINE_MAP)
    {
        //empty method
    }
};


class OP_MINUS: public  Calc_op
{
public:
    void vipolnit(list<string>* ARGS, stack<double>* NUM_STACK, map<string,double>* DEFINE_MAP);
};

class OP_SQRT: public  Calc_op
{
public:
    void vipolnit(list<string>* ARGS, stack<double>* NUM_STACK, map<string,double>* DEFINE_MAP);
};


class OP_MUL: public  Calc_op
{
public:
    void vipolnit(list<string>* ARGS, stack<double>* NUM_STACK, map<string,double>* DEFINE_MAP);
};

class OP_DIV: public  Calc_op
{
public:
    void vipolnit(list<string>* ARGS, stack<double>* NUM_STACK, map<string,double>* DEFINE_MAP);
};


class OP_DEFINE: public  Calc_op ////////////////////////////////////////
{
public:
    void vipolnit(list<string>* ARGS, stack<double>* NUM_STACK, map<string,double>* DEFINE_MAP);
};
#endif //LAB2_CALC_OPERATIONS_H
